import java.util.*;

class Account{
	protected double balance;
	
	public double credit(int amount) {
		balance=balance+amount;
		return balance;
	}
	
	public double debit(int amount) {
		balance=balance-amount;
		return balance;
	}
}

class SavingAccount extends Account{
	private double interestRate;
	public void setInterestRate() {
		interestRate=0.005;
	}
	public double getInterestRate() {
		return  interestRate;
	}
	public void calculateInterest() {
		balance=balance+(balance*getInterestRate());
		System.out.println("存款餘額為"+balance);
	}
}

class CheckAccount extends Account{
	private double transactionFee;
	public double credit(int amount) {
		balance=balance+amount-chargeFee(amount);
		return balance;
	}
	
	public double debit(int amount) {
		balance=balance-amount-chargeFee(amount);
		return balance;
	} 
	
	public double chargeFee(int amount) {
		transactionFee=amount*0.005;
		return transactionFee;
	}
}

abstract class Animal{
	String name;
	public Animal() {
		name="animal";
	}
	public void setName(String n) {
		name=n;
	}
	public abstract void move();
	public abstract void sound();
}

class Account2 implements Comparable<Account2>{
	String name;
	private int balance;
	
	Account2(String name,int balance){
		this.name=name;
		this.balance=balance;
	}
	
	@Override
	public String toString() {
		return String.format("(%s,%d)",name ,balance);
	}

	@Override
	public int compareTo(Account2 O) {
		return this.balance- O.balance;
	}
}

public class main0717 {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		//作業1:銀行帳戶
		//===============一般帳戶====================//
		SavingAccount User=new SavingAccount();
		System.out.println("以下是一般帳戶區");
		System.out.println("請輸入您要存款的金額：");
		int A1=sc.nextInt();
		System.out.println("存款金額:"+A1);
		User.credit(A1);
		User.setInterestRate();
		System.out.println("利率:"+User.getInterestRate());
		User.calculateInterest();
		System.out.println("請輸入您要提款的金額：");
		int A2=sc.nextInt();
		System.out.println("提款金額:"+A2);
		User.debit(A2);
		User.setInterestRate();
		System.out.println("利率:"+User.getInterestRate());
		User.calculateInterest();
		//================支票=======================//
		System.out.println("以下是支票區");
		CheckAccount user=new CheckAccount();
		System.out.println("請輸入您要存款的金額：");
		int A3=sc.nextInt();
		System.out.println("存款金額:"+A3);
		System.out.println("手續費:"+user.chargeFee(A3));
		System.out.println("存款餘額為"+user.credit(A3));
		System.out.println("請輸入您要提款的金額：");
		int A4=sc.nextInt();
		System.out.println("存款金額:"+A4);
		System.out.println("手續費:"+user.chargeFee(A4));
		System.out.println("存款餘額為"+user.debit(A4));
		//作業2:Animal
		System.out.println("========這是分割線============");
		new Object() {
			public void move() {
				System.out.println("Move：walk!");
			} 
		}.move();
		new Object() {
			public void sound() {
				System.out.print("Sound：rrrrr");
			}
		}.sound();
		//作業3:追加作業
		List<Account2> accounts=Arrays.asList(
			new Account2("Mary",30),
			new Account2("Gary",78),
			new Account2("Tery",66),
			new Account2("Ben",1200),
			new Account2("Alice",18)
		);
		System.out.println();
		Collections.sort(accounts);
		System.out.println(accounts);
		accounts.sort(
			Comparator.<Account2,String>comparing(p->p.name)
		);
		System.out.println(accounts);
	}

}

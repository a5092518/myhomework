import java.util.Scanner;

public class main0715 {
	public static int Jud(int x[],int y[]){ //x->答案   y->輸入的數字
		int A=0,B=0;
		for(int i=0;i<y.length;i++) {
			for(int j=0;j<x.length;j++) {
				if(y[i]==x[j]&&j!=i) B++;
				if(x[i]==y[j]&&i==j) A++;
			}
		}
		if(A>=4) {
			System.out.println("4A!恭喜你回答正確!");
		}else {
			System.out.println(A+"A"+B+"B");
		}
		return A;
	}
	public static void main(String[] args) {
//		// TODO Auto-generated method stub
		System.out.println("Hello World!");
		Scanner sc = new Scanner(System.in);
		//第一題:迴圈練習
		System.out.println("請輸入一個正整數:");
		int n=sc.nextInt();
		if(n>0){
			for(int i=1;i<=n;i++) {
				for(int j=n;j>0;j--) {
					if(j>i) {
						System.out.print(" ");
					}else {
						System.out.print("*");
					}
				}
				System.out.println();
			}
		}else{
			System.out.println("輸入錯誤!");
		}
		//第二題:終極密碼
		int ans=(int)(Math.random()*100+1);
		int min=1,max=100;
		System.out.println(ans);
		System.out.println("請猜一個數字：");
		int userAns=sc.nextInt();
		if(userAns>0) {
			while(userAns!=ans) {
				if(userAns>ans) {
					max=userAns-1;
					if(max==ans&&min==ans) {
						System.out.println("因為只剩一個數字，所以你輸了喔!答案是："+ans);
						break;
					}
					System.out.println("太大了!");
					System.out.println(min+"~"+max+"請重新猜一次：");
					userAns=sc.nextInt();
				}else if(userAns<ans) {
					min=userAns+1;
					if(min==ans&&max==ans) {
						System.out.println("因為只剩一個數字，所以你輸了喔!答案是："+ans);
						break;
					}
					System.out.println("太小了!");
					System.out.println(min+"~"+max+"請重新猜一次：");
					userAns=sc.nextInt();
				}
			}
			if(userAns==ans) System.out.println("恭喜你答對了!");
		}else {
			System.out.println("您輸入的數字需大於0!");
		}
		//第三題 陣列內容反轉
		System.out.println("輸入一個數字：");
		int b=sc.nextInt();
		int [] arr=new int[b];
		int [] arr1=new int[b];
		for(int i=0;i<arr.length;i++) arr[i]=(int)(Math.random()*100+1); 
		for(int i=0;i<arr.length;i++) {
			int a=(int)(arr.length-1)-i;
			arr1[a]=arr[i];
		}
		for(int i=0;i<arr.length;i++) System.out.print(arr[i]+" ");
		System.out.println();
		for(int i=0;i<arr.length;i++) System.out.print(arr1[i]+" ");
	//========================================================================//
		//博智作業1:輸入兩副牌比大小
//			System.out.println("第一個字元(花色):黑桃->S、紅心->H、方塊->D、梅花->C");
//			System.out.println("第二個字元(數字):A、2、3、4、5、6、7、8、9、T、J、Q、K");
//			System.out.println("請輸入第一副的五張鋪克牌：");
//			String [] o=new String [5];
//			for(int i=0;i<5;i++) o[i]=sc.next();
//			String [] Opair=new String [10];
//			for(int i=0;i<5;i++) {
//				int a=i*2;
//				Opair[a]=o[i].substring(0, 1);
//				Opair[a+1]=o[i].substring(1);
//			}
//			for(int i=0;i<Opair.length;i++) System.out.print(Opair[i]+" ");
//			System.out.println();
//			System.out.println("請輸入第二副的五張鋪克牌：");
//			String [] s=new String [5];
//			for(int i=0;i<5;i++) s[i]=sc.next();
//			String [] Spair=new String [10];
//			//=================================//
//			//1->同花順、2->鐵支、3->葫蘆、4->順子、5->兩對、6->一對、7->散排
//			int type=0;
//			//=================================//
//			for(int i=0;i<5;i++) {
//				int b=i*2;
//				Spair[b]=s[i].substring(0, 1);
//				Spair[b+1]=s[i].substring(1);
//			}
//			for(int i=0;i<Spair.length;i++) System.out.print(Spair[i]+" ");
//			String color[]= {"S","H","D","C"};
//			String number[]= {"A","1","2","3","4","5","6","7","8","9","T","J","Q","K"};
//			int q=0;
//			for(int i=1;i<6;i+=2) {
//				for(int j=5;j>0;j-=2) {
//					if(Opair[i]==Opair[j]&&i!=j) q++;
//				}
//				if(q==4) type=2;
//					
//				if(q==0) {
//					
//				}
//			}
		//博智作業2:攝氏溫度和華氏溫度轉換
			System.out.println("請輸入一個數字：");
			float temp=sc.nextFloat();
			float Ctemp,Ftemp;
			Ftemp=temp*9/5+32;
			Ctemp= (temp-32)*5/9;
			System.out.println("攝氏溫度"+temp+"℃ = 華氏溫度"+Ftemp+"℉");
			System.out.println("華氏溫度"+temp+"℉ = 攝氏溫度"+Ctemp+"℃");
		//博智作業3:1A2B猜4位數字
			int [] ANS=new int[4];
			ANS[0]=(int)(Math.random()*9+1);
			ANS[1]=(int)(Math.random()*10);
			ANS[2]=(int)(Math.random()*10);
			ANS[3]=(int)(Math.random()*10);
			while(ANS[0]==ANS[1]) {
				ANS[1]=(int)(Math.random()*10);
			}
			while(ANS[0]==ANS[2]||ANS[1]==ANS[2]) {
				ANS[2]=(int)(Math.random()*10);
			} 
			while(ANS[0]==ANS[3]||ANS[1]==ANS[3]||ANS[2]==ANS[3]) {
				ANS[3]=(int)(Math.random()*10);
			} 
			System.out.println(ANS[0]+" "+ANS[1]+" "+ANS[2]+" "+ANS[3]);
			int check=0,Uans=0;
			int [] UANS=new int[4];
			while(check!=4) {
				System.out.println("請輸入四個【不同】數字的字串：");
				Uans=sc.nextInt();
				while(Uans>9876||Uans<1023) {
					System.out.println("輸入錯誤!請重新輸入:");
					Uans=sc.nextInt();
				}
				UANS[0]=Uans/1000;
				UANS[1]=(Uans%1000)/100;
				UANS[2]=(Uans%100)/10;
				UANS[3]=Uans%10;
				check=0;
				check=Jud(ANS,UANS);
			}
	}

}

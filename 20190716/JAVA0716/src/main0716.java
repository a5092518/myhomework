import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.LinkedList;
class BigNumber{
	public BigNumber(String x,String y) {
		String a =x.substring(x.length()-1,x.length());
		String b =y.substring(y.length()-1,y.length());
		ArrayList<Integer> arr1 = new ArrayList<Integer>();
		ArrayList<Integer> arr2 = new ArrayList<Integer>();
		for(int i=x.length()-1;i>0;i--) a+=x.substring(i-1,i);
		for(int i=y.length()-1;i>0;i--) b+=y.substring(i-1,i);
		for(int i=0;i<a.length();i++) {
			String a1=null;
			if(i+1<=a.length()) a1=a.substring(i,i+1);
			else if(i+1>a.length()&&i==a.length()) a1=a.substring(i);
			int s= Integer.valueOf(a1);
			arr1.add(s);
		}
		for(int i=0;i<b.length();i++) {
			String b1=null;
			if(i+1<=b.length()) b1=b.substring(i,i+1);
			else if(i+1>b.length()&&i==b.length()) b1=b.substring(i);
			int s= Integer.valueOf(b1);
			arr2.add(s);
		}
		ArrayList<Integer> arrs = new ArrayList<Integer>();
		int na=arr1.size(),nb=arr2.size(),o=0,sum=0,c=0;
		if(na>=nb) {
			for(int i=0;i<na;i++) {
				if(i<nb) sum=arr1.get(i)+arr2.get(i)+o;
				else sum=arr1.get(i)+o;
				o=sum/10;
				if(sum>=10) { 	
					if(i==na-1) {
						sum=sum%10;
						arrs.add(sum);	
						arrs.add(o);
					}else {
						sum=sum%10;
						arrs.add(sum);
					}
				}else arrs.add(sum);
			}
			System.out.print("相加結果為");
			for(int i=arrs.size()-1;i>=0;i--) System.out.print(arrs.get(i));
			System.out.println();
		}else {
			for(int i=0;i<nb;i++) {
				if(i<na) sum=arr1.get(i)+arr2.get(i)+o;
				else sum=arr2.get(i)+o;
				o=sum/10;
				if(sum>=10) { 	
					if(i==nb-1) {
						sum=sum%10;
						arrs.add(sum);	
						arrs.add(o);
					}else {
						sum=sum%10;
						arrs.add(sum);
					}
				}else arrs.add(sum);
			}
			System.out.print("相加結果為");
			for(int i=arrs.size()-1;i>=0;i--) System.out.print(arrs.get(i));
			System.out.println();
		}
	//減法
		o=0;
		ArrayList<Integer> arrm = new ArrayList<Integer>();
		if(na>=nb) {
			for(int i=0;i<na;i++) {
				if(i<nb) sum=arr1.get(i)-arr2.get(i)-o;
				else sum=arr1.get(i);
				if(sum<0) {
					sum+=10;
					o=1;
					arrm.add(sum);
				}else {
					arrm.add(sum);
					o=0;
				}
			}
			System.out.print("相減結果為");
			for(int i=arrm.size()-1;i>=0;i--) System.out.print(arrm.get(i));
		}else {
			for(int i=0;i<nb;i++) {
				if(i<na) sum=arr2.get(i)-arr1.get(i)-o;
				else sum=arr2.get(i);
				if(sum<0) {
					sum+=10;
					o=1;
					arrm.add(sum);
				}else {
					arrm.add(sum);
					o=0;
				}
			}
			System.out.print("相減結果為");
			for(int i=arrm.size()-1;i>=0;i--) System.out.print(arrm.get(i));
		}
	}
}
class two{
	private LinkedList<String> link;
	private ArrayList<String> arrayList;
	public two(){
		link = new LinkedList<String>();
		arrayList = new ArrayList<String>();
	}
	public void put(String name){
		link.add("這是第一筆資料");
		link.add("這是第二筆資料");
		link.add("這是第三筆資料");
        link.addFirst(name);
        arrayList.add("這是第一筆資料");
		arrayList.add("這是第二筆資料");
		arrayList.add("這是第三筆資料");
		arrayList.add(name);
    }
	public void show() {
		System.out.println("堆疊：");
		System.out.println(arrayList);
		System.out.println("==========這是分隔線============");
		System.out.println("佇列：");
		System.out.print(link);
	}
}
class three{
	private HashMap<String,String> map;
	private HashMap<String,Integer> time;
	public three(){
		map=new HashMap<String,String>();
		time=new HashMap<String,Integer>();
		map.put("東京迪士尼", "(35°37N,139°52E)");
		map.put("台北101", "(25°02N,121°33E)");
		map.put("法國艾菲爾鐵塔", "(48°51N,2°17E)");
		map.put("美國黃石國家公園", "(44°36N,110°30W)");
		time.put("東京迪士尼", 9);
		time.put("台北101", 8);
		time.put("法國艾菲爾鐵塔", 2);
		time.put("美國黃石國家公園", (-4));
	}
	public void Map(String way) {
		String w=map.get(way);
		System.out.println("經緯度為"+w);
	}
	public void Time(String w1,String w2) {
		int t1=time.get(w1);
		int t2=time.get(w2);
		int sum=0;
		if(t1>t2) {
			sum=(t1-t2);
		}else if(t1<t2) {
			sum=(t2-t1);
		}
		System.out.print("時差為"+sum+"H");
	}
}
public class main0716 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		//作業1
		System.out.println("請輸入兩組數字：");
		String a=sc.next();
		String b=sc.next();
		BigNumber B=new BigNumber(a,b);
//		//作業2
		System.out.println();
		System.out.println("輸入隨意的字串：");
		String d=sc.next();	
		two Two=new two();
		Two.put(d);
		Two.show();
		//作業3
		System.out.println();
		System.out.println("請輸入一個地名：");
		String w=sc.next();
		three Three=new three();
		Three.Map(w);
		System.out.println();
		System.out.println("請輸入兩個地名：");
		String w1=sc.next();
		String w2=sc.next();
		Three.Time(w1,w2);
	}
}

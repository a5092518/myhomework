package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    Button b1;Button b2;Button b3;Button b4;Button b5;
    Button b6;Button b7;Button b8;Button b9;Button b10;
    Button b11;Button b12;Button b13;Button b14;Button b15;
    Button b16;Button b17;
    static TextView fm;TextView ans;
    static String all="";
    static ArrayList<Integer> num ;
    static ArrayList<Integer> sign ;
    static double ANS;
    public static void getb(final Button b){

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                all+=b.getText().toString();
                fm.setText(fm.getText().toString()+b.getText().toString());
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //數字
        b1=findViewById(R.id.b1);b2=findViewById(R.id.b2);b3=findViewById(R.id.b3);
        b4=findViewById(R.id.b4);b5=findViewById(R.id.b5);b6=findViewById(R.id.b6);
        b7=findViewById(R.id.b7);b8=findViewById(R.id.b8);b9=findViewById(R.id.b9);b11=findViewById(R.id.b11);
        //結束
        b12=findViewById(R.id.b12);
        //計算符號
        b13=findViewById(R.id.b13);b14=findViewById(R.id.b14);b15=findViewById(R.id.b15);
        b16=findViewById(R.id.b16);b17=findViewById(R.id.b17);

        fm=findViewById(R.id.fm);ans=findViewById(R.id.ans);

        getb(b1);getb(b2);getb(b3);getb(b4);getb(b5);
        getb(b6);getb(b7);getb(b8);getb(b9);getb(b11);

        num=new ArrayList();
        sign=new ArrayList();

        b13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=Integer.valueOf(all);
                num.add(n);
                sign.add(1);
                String s=fm.getText().toString()+"+";
                fm.setText(s);
                all="";
            }
        });
        b14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=Integer.valueOf(all);
                num.add(n);
                sign.add(2);
                String s=fm.getText().toString()+"-";
                fm.setText(s);
                all="";
            }
        });
        b15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=Integer.valueOf(all);
                num.add(n);
                sign.add(3);
                String s=fm.getText().toString()+"×";
                fm.setText(s);
                all="";
            }
        });
        b16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=Integer.valueOf(all);
                num.add(n);
                sign.add(4);
                String s=fm.getText().toString()+"÷";
                fm.setText(s);
                all="";
            }
        });
        b17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=Integer.valueOf(all);
//                ans.setText(all);
                num.add(n);
                ANS=num.get(0);
                for(int i=0;i<sign.size();i++){
                   switch(sign.get(i)){
                       case 1:
                           ANS+=num.get(i+1);
                       case 2:
                           ANS-=num.get(i+1);
                       case 3:
                           ANS*=num.get(i+1);
                       case 4:
                           ANS/=num.get(i+1);
                   }
                }
                String A=Double.toString(ANS);
                ans.setText(A);
            }
        });
    }
}

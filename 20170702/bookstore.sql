-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2019 年 07 月 02 日 08:10
-- 伺服器版本: 10.1.37-MariaDB
-- PHP 版本： 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `bookstore`
--
CREATE DATABASE IF NOT EXISTS `bookstore` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bookstore`;

-- --------------------------------------------------------

--
-- 資料表結構 `inventory`
--

CREATE TABLE `inventory` (
  `name` varchar(30) NOT NULL,
  `ItQ` int(10) NOT NULL,
  `lastdate` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `inventory`
--

INSERT INTO `inventory` (`name`, `ItQ`, `lastdate`) VALUES
('123', 0, '2019-07-02'),
('滿清十大酷刑', 17, '2018-12-30'),
('農業經濟學', 10, '2019-07-03'),
('遇見幸福', 13, '2019-07-02'),
('過暑假的一百種玩法', 16, '2019-07-09'),
('駕照很難考', 29, '2019-08-24');

-- --------------------------------------------------------

--
-- 資料表結構 `purchase`
--

CREATE TABLE `purchase` (
  `name` varchar(30) NOT NULL,
  `PcQ` int(10) NOT NULL,
  `Pdate` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `purchase`
--

INSERT INTO `purchase` (`name`, `PcQ`, `Pdate`) VALUES
('滿清十大酷刑', 10, '2018-12-30'),
('遇見幸福', 5, '2019-05-20'),
('駕照很難考', 10, '2019-07-02'),
('遇見幸福', 3, '2019-07-02'),
('農業經濟學', 6, '2019-07-17'),
('過暑假的一百種玩法', 20, '2019-07-09'),
('駕照很難考', 4, '2019-07-30'),
('農業經濟學', 10, '2019-07-03'),
('123', 123, '2019-07-02'),
('駕照很難考', 15, '2019-08-24');

-- --------------------------------------------------------

--
-- 資料表結構 `shipment`
--

CREATE TABLE `shipment` (
  `name` varchar(30) NOT NULL,
  `SmQ` int(10) NOT NULL,
  `Sdate` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `shipment`
--

INSERT INTO `shipment` (`name`, `SmQ`, `Sdate`) VALUES
('滿清十大酷刑', 3, '2019-07-02'),
('農業經濟學', 2, '2018-11-20'),
('遇見幸福', 4, '2019-03-14'),
('過暑假的一百種玩法', 4, '2019-07-10'),
('123', 23, '2019-07-17');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

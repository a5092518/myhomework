<?php
	/*function create_connection()
	{
		$link=mysqli_connect("localhost","root","") or die("無法顯示連接：".mysqli_connect_error());
		mysqli_query($link,"set names utf8");
		return $link;
	}
	function execute_sql($link,$database,$sql)
	{
		mysqli_select_db($link,$database) or die("開啟資料庫失敗：".mysqli_error($link));
		$result=mysqli_query($link,$sql);
		return $result;
	}*/
	$servername="localhost";
	$username="root";
	$password="";
	$db="bookstore";
	//創建連接
	$conn=mysqli_connect($servername,$username,$password,$db);
	//------------------------------------------------------------------
	mysqli_query($conn , "set names utf8");
	//require_once("dbtools.inc.php");
	$fun=$_POST['fun'];
	if($fun=="進貨"){
		$date=date("Y-m-d");
		echo "<h2>輸入進貨單：</h2>";
		echo "<form method='POST' action='book-2.php'>";
			echo "書名：<input type='text' name='name'><p><p>";
			echo "數量：<input type='text' name='amount'><p><p>";
			echo "日期：<input type='date' name='date' value='$date' min='$date'><p><p>";
			echo "<input type='submit' name='enter' value='送出進貨單'>";
		echo "</form>";
	}elseif($fun=="出貨"){
		$date=date("Y-m-d");
		echo "<h2>輸入出貨單：</h2>";
		echo "<form method='POST' action='book-2.php'>";
			$sql="SELECT `name` FROM `inventory`";
			$i=mysqli_query($conn,$sql);
			echo "書名：<select name=\"item\">";
			while($irow=mysqli_fetch_row($i)){
				echo "<option>".$irow[0]."</option>";
			}	
			echo "</select><p><p>";
			echo "數量：<input type='text' name='amount'><p><p>";
			echo "日期：<input type='date' name='date' value='$date' min='$date'><p><p>";
			echo "<input type='submit' name='enter' value='送出出貨單'>";
		echo "</form>";
	}elseif($fun=="查詢"){
		echo "<h2>請選擇排序方法：</h2>";
		echo "<form method='POST' action='book-2.php'>";
			echo "<input type='submit' name='enter' value='庫存數量'><p><p>";
			echo "<input type='submit' name='enter' value='進貨日期'>";
		echo "</form>";
	}elseif($fun=="刪除"){
		echo "<h2>請選擇要刪除的項目：</h2>";
		echo "<form method='POST' action='book-2.php'>";
			$sql="SELECT `name` FROM `inventory`";
			$i=mysqli_query($conn,$sql);
			echo "書名：<select name=\"item\">";
			while($irow=mysqli_fetch_row($i)){
				echo "<option>".$irow[0]."</option>";
			}	
			echo "</select><p><p>";
			echo "<input type='submit' name='enter' value='確定'>";
		echo "</form>";
	}
?>
<?php
	$servername="localhost";
	$username="root";
	$password="";
	$db="bookstore";
	//創建連接
	$conn=mysqli_connect($servername,$username,$password,$db);
	//------------------------------------------------------------------
	mysqli_query($conn , "set names utf8");
	//require_once("dbtools.inc.php");
	$enter=$_POST['enter'];
	//=====進貨======//
	if($enter=="送出進貨單"){
		$name=$_POST['name'];
		$amount=$_POST['amount'];
		$date=$_POST['date'];
		$sql="SELECT * FROM `inventory` WHERE `name`='$name'";
		$all=mysqli_query($conn,$sql);
		$arow=mysqli_fetch_row($all);
		if($arow==""){
			$sql="INSERT INTO `inventory`(`name`, `ItQ`,`lastdate`) VALUES ('$name','$amount','$date')";
			mysqli_query($conn,$sql);
			$sql1="INSERT INTO `purchase`(`name`, `PcQ`,`Pdate`) VALUES ('$name','$amount','$date')";
			mysqli_query($conn,$sql1);
			echo "<h2>進貨單：</h2>";
			echo "<table border='1'>";
				echo "<tr>";
					echo "<th>書名</th><th>數量</th><th>進貨日期</th>";
				echo "</tr>";
				echo "<tr>";
					echo "<td>".$name."</td>"."<td>".$amount."</td>"."<td>".$date."</td>";
				echo "</tr>";
			echo "</table><p>";
			$sql2="SELECT * FROM `inventory` WHERE `name`='$name'";
			$A=mysqli_query($conn,$sql2);
			$Arow=mysqli_fetch_row($A);
			echo "<h2>庫存表：</h2>";
			echo "<table border='1'>";
				echo "<tr>";
					echo "<th>書名</th><th>數量</th><th>最後進貨時間</th>";
				echo "</tr>";
				echo "<tr>";
					echo "<td>".$Arow[0]."</td>"."<td>".$Arow[1]."</td>"."<td>".$Arow[2]."</td>";
				echo "</tr>";
			echo "</table><p>";
		}else{
			$sql="SELECT `ItQ` FROM `inventory` WHERE `name`='$name'";
			$Q=mysqli_query($conn,$sql);
			$qrow=mysqli_fetch_row($Q);
			$n=(int)$qrow[0]+(int)$amount;
			$sql="UPDATE `inventory` SET `ItQ`='$n',`lastdate`='$date' WHERE `name`='$name'";	
			mysqli_query($conn,$sql);
			$sql2="INSERT INTO `purchase`(`name`, `PcQ`,`Pdate`) VALUES ('$name','$amount','$date')";
			mysqli_query($conn,$sql2);
			echo "<h2>進貨單：</h2>";
			echo "<table border='1'>";
				echo "<tr>";
					echo "<th>書名</th><th>數量</th><th>進貨日期</th>";
				echo "</tr>";
				echo "<tr>";
					echo "<td>".$name."</td>"."<td>".$amount."</td>"."<td>".$date."</td>";
				echo "</tr>";
			echo "</table><p>";
			$sql3="SELECT * FROM `inventory` WHERE `name`='$name'";
			$A=mysqli_query($conn,$sql3);
			$Arow=mysqli_fetch_row($A);
			echo "<h2>庫存表：</h2>";
			echo "<table border='1'>";
				echo "<tr>";
					echo "<th>書名</th><th>數量</th><th>最後進貨時間</th>";
				echo "</tr>";
				echo "<tr>";
					echo "<td>".$Arow[0]."</td>"."<td>".$Arow[1]."</td>"."<td>".$Arow[2]."</td>";
				echo "</tr>";
			echo "</table><p>";
		}
		
	}
	//=====出貨======//
	if($enter=="送出出貨單"){
		$item=$_POST['item'];
		$n=$_POST['amount'];
		$date=$_POST['date'];
		$sql="INSERT INTO `shipment`(`name`, `SmQ`, `Sdate`) VALUES ('$item','$n','$date')";
		mysqli_query($conn,$sql);
		$sql1="SELECT `ItQ` FROM `inventory` WHERE `name`='$item'";
		$Q=mysqli_query($conn,$sql1);
		$qrow=mysqli_fetch_row($Q);
		$amount=(int)$qrow[0]-(int)$n;
		$sql2="UPDATE `inventory` SET `ItQ`='$amount' WHERE `name`='$item'";
		mysqli_query($conn,$sql2);
		echo "<h2>出貨單：</h2>";
		echo "<table border='1'>";
			echo "<tr>";
				echo "<th>書名</th><th>數量</th><th>出貨日期</th>";
			echo "</tr>";
			echo "<tr>";
				echo "<td>".$item."</td>"."<td>".$n."</td>"."<td>".$date."</td>";
			echo "</tr>";
		echo "</table><p>";
		$sql3="SELECT * FROM `inventory` WHERE `name`='$item'";
		$all=mysqli_query($conn,$sql3);
		$arow=mysqli_fetch_row($all);
		echo "<h2>庫存表：</h2>";
		echo "<table border='1'>";
			echo "<tr>";
				echo "<th>書名</th><th>數量</th><th>最後進貨時間</th>";
			echo "</tr>";
			echo "<tr>";
				echo "<td>".$arow[0]."</td>"."<td>".$arow[1]."</td>"."<td>".$arow[2]."</td>";
			echo "</tr>";
		echo "</table><p>";
	}
	//=====查詢======//
	if($enter=="庫存數量"){
		$sql="SELECT * FROM `inventory` ORDER BY `inventory`.`ItQ` DESC";
		$all=mysqli_query($conn,$sql);
		echo "<h2>庫存表：</h2>";
		echo "<table border='1'>";
			echo "<tr>";
				echo "<th>書名</th><th>數量</th><th>最後進貨時間</th>";
			echo "</tr>";
			while($arow=mysqli_fetch_row($all)){
				echo "<tr>";
					echo "<td>".$arow[0]."</td>"."<td>".$arow[1]."</td>"."<td>".$arow[2]."</td>";
				echo "</tr>";
			}
		echo "</table><p>";
		
	}elseif($enter=="進貨日期"){
		$sql="SELECT * FROM `inventory` ORDER BY `inventory`.`lastdate` DESC";
		$all=mysqli_query($conn,$sql);
		echo "<h2>庫存表：</h2>";
		echo "<table border='1'>";
			echo "<tr>";
				echo "<th>書名</th><th>數量</th><th>最後進貨時間</th>";
			echo "</tr>";
			while($arow=mysqli_fetch_row($all)){
				echo "<tr>";
					echo "<td>".$arow[0]."</td>"."<td>".$arow[1]."</td>"."<td>".$arow[2]."</td>";
				echo "</tr>";
			}
		echo "</table><p>";
	}
	//======刪除=====//
	if($enter=="確定"){
		$item=$_POST['item'];
		$n='0';
		$sql="UPDATE `inventory` SET `ItQ`='$n' WHERE `name`='$item'";
		mysqli_query($conn,$sql);
		$sql1="SELECT * FROM `inventory` WHERE `name`='$item'";
		$all=mysqli_query($conn,$sql1);
		$arow=mysqli_fetch_row($all);
		echo "<h2>庫存表：</h2>";
		echo "<table border='1'>";
			echo "<tr>";
				echo "<th>書名</th><th>數量</th>";
			echo "</tr>";
			echo "<tr>";
				echo "<td>".$arow[0]."</td>"."<td>".$arow[1]."</td>";
			echo "</tr>";
		echo "</table><p>";
	}
?>
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class client2 {
	public static void main(String[] args) {
		try {
			Socket socket =new Socket("127.0.0.1",8005);
			
			DataInputStream intstream=new DataInputStream(socket.getInputStream());
			DataOutputStream outstream=new DataOutputStream(socket.getOutputStream());
			Scanner sc=new Scanner(System.in);
			
			while(true) {
				//傳送訊息的部分
				System.out.println("請輸入四個數字：");
				String sendmsg=sc.next();
				while(sendmsg.length()!=4) {
					if(sendmsg.length()<4) {
						System.out.println("數字輸入須為4碼!");
						System.out.println("請輸入四個數字：");
						sendmsg=sc.next();
					}else if(sendmsg.length()>4) {
						System.out.println("數字輸入須為4碼!");
						System.out.println("請輸入四個數字：");
						sendmsg=sc.next();
					}
				}
				System.out.println("client："+sendmsg);
				outstream.writeUTF(sendmsg);
				
				//收訊息的部分
				String getmsg = intstream.readUTF();	//傳送文字用UTF
				System.out.println("Server:"+getmsg);
				if(getmsg.equals("恭喜你獲勝了")) break;
			}
				//結束
				socket.close();
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

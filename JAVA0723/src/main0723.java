import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class main0723 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		try {
//			Socket socket = new Socket("www.google.tw",80);
//			System.out.println("本幾位址："+socket.getLocalAddress());
//			System.out.println("本機port："+socket.getLocalPort());
//			System.out.println("遠端位址："+socket.getInetAddress());
//			System.out.println("遠端port："+socket.getPort());
//		}catch(IOException e) {
//			System.out.println("錯誤!");
//		}
		
		try {
			ServerSocket ssk =new ServerSocket(8004);
			System.out.println("開始傾聽...");
			Socket sk= ssk.accept();	//接收到客戶端連線
			System.out.println("已經有客戶端連線");
			
			DataInputStream inS=new DataInputStream(sk.getInputStream());
			DataOutputStream outS = new DataOutputStream(sk.getOutputStream());
			Scanner sc=new Scanner(System.in);
			
			while(true) {
				//收訊息的部分
				String getmsg = inS.readUTF();	//傳送文字用UTF
				System.out.println("Client:"+getmsg);
				if(getmsg.equals("stop")) break;
				//傳送訊息的部分
				System.out.println("請輸入訊息：");
				String sendmsg=sc.next();		
				System.out.println("Server："+sendmsg);
				outS.writeUTF(sendmsg);	
				if(sendmsg.equals("stop")) break;
			}
			//結束
			sk.close();
			ssk.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

}

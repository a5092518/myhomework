<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<style>
		footer{
			background-color:#666666;
			height:500px;
			text-align:center;
			color:white;
		}
	</style>
    <title>Acer臺灣</title>
  </head>
  <body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12" style="padding:0;">
				<img src="bg-Make_Your_Mark-L.png" width="100%" height="90%">
			</div>
			<div class="col-12 col-md-12 col-lg-12" style="padding:0;">
				<img src="bg-Acer_Homepage_2019_SYS-e.png" width="100%" height="90%">
			</div>
			
			<div class="col-6 col-md-6 col-lg-3" style="padding:0;">
				<img src="Swift7_Ale.png" width="100%" height="85%">
				<h3 align="center">Swift7</h3>
			</div>
			<div class="col-6 col-md-6 col-lg-3" style="padding:0;">
				<img src="swift_5.png" width="100%" height="85%">
				<h3 align="center">Swift5</h3>
			</div>
			<div class="col-6 col-md-6 col-lg-3" style="padding:0;">
				<img src="Nitro50.png" width="100%" height="85%">
				<h3 align="center">Nitro50</h3>
			</div>
			<div class="col-6 col-md-6 col-lg-3" style="padding:0;">
				<img src="Aspire_S24.png" width="100%" height="85%">
				<h3 align="center">AspireS24</h3>
			</div>
			<div class="col-12 col-md-6 col-lg-3" style="padding:0;">
				<img src="P1.png" width="100%" height="100%">
			</div>
			<div class="col-12 col-md-6 col-lg-3" style="padding:0;">
				<img src="P2.jpg" width="100%" height="100%">
			</div>
			<div class="col-12 col-md-6 col-lg-3" style="padding:0;">
				<img src="P3.png" width="100%" height="100%">
			</div>
			<div class="col-12 col-md-6 col-lg-3" style="padding:0;">
				<img src="P4.png" width="100%" height="100%">
			</div>
		</div>
	</div>
	<footer class="footer" >
		<BR>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3" style="padding:0;">
					<B color="white">關於ACER</B><br><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">聯絡我們</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">投資者關係</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">新聞</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">企業社會責任</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">招兵買馬</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">獎項</a>
				</div>
				<div class="col-lg-3" style="padding:0;">
					<B color="white">服務</B><br><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">ACERID</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">驅動程式與說明手冊</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">疑難排解文章</a>
					
				</div>
				<div class="col-lg-3" style="padding:0;">
					<B color="white">資訊</B><br><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">創新</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">產品註冊</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">PREDATOR</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">NEXT@ACER</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">ACER DESIGN</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">銷售地點</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">軟體</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">價格表</a>
				</div>
				<div class="col-lg-3" style="padding:0;">
					<B color="white">法律聲明</B><br><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">隱私權聲明</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">法律聲明</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">宏碁股份有限公司營業規章</a><br><br>
					<a href="0703-3.php" style="text-decoration:none;color:#DDDDDD;font-size:14px;">其他法律資訊</a><br><br>	
				</div>
			</div>
		</div>
	</footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
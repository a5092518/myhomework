import java.util.Scanner;
//剩下判斷失敗，輸出數字大小:level
public class game2048 {
	static Thread thread=new gametime();
    public static void main(String[] args) {
        // write your code here
        block blocks=new block();
        String m="";
        Scanner sc=new Scanner(System.in);
        thread.start();
        do {
            do {
                System.out.println( "請輸入移動方向:" );
                m = sc.next();
            } while (m.equals("d") && m.equals("u") && m.equals("l") && m.equals("r"));
        }while ( !blocks.flow(m) );
        if(blocks.checklose()) {
        	blocks.show();
        	System.out.println("你輸了這場遊戲！");
        	thread.interrupt();
        }
        if(blocks.win()) {
        	blocks.show();
        	System.out.println("恭喜你獲勝！");
        	thread.interrupt();
        }
}
    public static class block{
        private int [][] b=new int[4][4];   //一直變動的
        private int much=2;   //產生多少數字初始值為二，在MakeNumber()處理  ps這裡還沒做空格不夠時的處理
        private int level=1;  //產生最大的數字，在MakeNumber()處理  ps這裡還沒做隨數字大，產生數字大的處理時的處理

        private int [][] oldb=new int[4][4]; //目前遊玩狀態
        //開始遊戲，產生數字
        public block(){
            for( int y=0 ; y <4 ; y++ ){
                for( int z=0 ; z <4 ; z++ ){
                    b[y][z]=0;
                }
            }
            MakeNumber();
            SaveOld();
            show();
        }
        //管理遊戲流程 出現輸贏為true
        public boolean flow( String m ){
            //System.out.print( "flow" );
            move( m );
            MakeNumber();
            SaveOld();
            if( checklose() ) return true;
            if( win() ) return true;
            reOld();
            show();
            return false;
        }
        //秀數字
        public void show(){
        	String sendmsg="";
            //System.out.print( "show" );
            for( int y=0 ; y <4 ; y++ ){
                for( int z=0 ; z <4 ; z++ ){
                    //System.out.print( b[y][z] +"  ");
                    System.out.printf("%5ud", b[y][z]);   
                }
                System.out.println( );
            }
//            sendmsg=Integer.toString(b[0][0])+",";
//            for( int y=0 ; y <4 ; y++ ){
//                for( int z=0 ; z <4 ; z++ ){
//                	sendmsg=sendmsg+Integer.toString(b[y][z])+",";
//                }
//    		}
            //System.out.println(sendmsg);
        }
        //移動
        public void move( String m ){
           // System.out.print( "move" );
            int ypm=0,y=0,yend=0,x=0,xpm=0,xend=0,z=0;  //pm為檢查時移動距離 正負為檢查方向  end為檢查終點
            boolean xy=true;  //true上下移動 false左右移動
            if( m.equals( "u" ) ){
                ypm=1;y=0;yend=4;x=0;xpm=0;xy=true;
            }else if( m.equals("d") ){
                ypm=-1;y=3;yend=-1;x=0;xpm=0;xy=true;
            }else if( m.equals("l") ){
                ypm=0;y=0;x=0;xpm=1;xend=4;xy=false;
            }else if( m.equals("r")){
                ypm=0;y=0;x=3;xpm=-1;xend=-1;xy=false;
            }


            boolean ok=false;  //檢查是否完成
            do{
                do{
                    if(b[y][x]==0){    //目前為0
                    	//System.out.print("一"+ypm+"二"+xpm);
                    	if(y+ypm>3||x+xpm>3) break;
                        if(b[y+ypm][x+xpm]==0){  //接下來的移動位置為0
                            if(xy){   //上下移動檢查
                                if(ypm>0)ypm++;
                                else ypm--;
                                if(y+ypm==yend){
                                    ok=true;
                                }
                            }else{    //左右移動檢查
                                if(xpm>0)xpm++;
                                else xpm--;
                                if(x+xpm==xend){
                                    ok=true;
                                }
                            }
                        }else{       //接下來的移動位置不為0
                            b[y][x]=b[y+ypm][x+xpm];   //change site
                            b[y+ypm][x+xpm]=0;
                            if(xy){
                                if(ypm>0)ypm++;
                                else ypm--;
                                if(y+ypm==yend){
                                    ok=true;
                                }
                            }else{
                                if(xpm>0)xpm++;
                                else xpm--;
                                if(x+xpm==xend){

                                    ok=true;
                                }
                            }
                        }
                    }else{     //目前位置不為0
//                    	System.out.println();
//                    	System.out.print("三"+ypm+"四"+xpm);
                    	if(y+ypm>3||x+xpm>3) break;
                        if(b[y+ypm][x+xpm]==0){ //要檢查的位置為0
                            if(xy){
                                if(ypm>0)ypm++;
                                else ypm--;
                                if(y+ypm==yend){
                                    ok=true;
                                }
                            }else{
                                if(xpm>0)xpm++;
                                else xpm--;
                                if(x+xpm==xend){

                                    ok=true;
                                }
                            }
                        }else if(b[y][x]==b[y+ypm][x+xpm]){  //要檢查的位置與目前位置相等
                            b[y][x]*=2;
                            b[y+ypm][x+xpm]=0;
                            if(xy){
                                if(ypm>0){
                                    y++;
                                    ypm=1;
                                }else{
                                    y--;
                                    ypm=-1;
                                    if(y+ypm==yend){
                                        ok=true;
                                    }
                                }
                            }else{
                                if(xpm>0){
                                    x++;
                                    xpm=1;
                                }else{
                                    x--;
                                    xpm=-1;
                                    if(x+xpm==xend){
                                        ok=true;
                                    }
                                }

                            }
                        }else if(b[y][x]!=b[y+ypm][x+xpm]){    //要檢查的位置與目前位置不相等
                            if(xy){
                                if(ypm>0){
                                    y++;
                                    ypm=1;
                                }else{
                                    y--;
                                    ypm=-1;
                                    if(y+ypm==yend){
                                        ok=true;
                                    }
                                }
                            }else{
                                if(xpm>0){
                                    x++;
                                    xpm=1;
                                }else{
                                    x--;
                                    xpm=-1;
                                    if(x+xpm==xend){
                                        ok=true;
                                    }
                                }
                            }

                        }

                    }

                }while(ok==false);  //當前位置為檢查完成
                if( m.equals( "u" )){   //換下個位置檢查前置動作
                    ypm=1;y=0;
                }else if( m.equals("d") ){
                    ypm=-1;y=3;
                }else if( m.equals("l") ){
                    x=0;xpm=1;
                }else if( m.equals("r") ){
                    x=3;xpm=-1;
                }
                ok=false;
                z++;  //z為檢查到第幾條
                if(xy){
                    x=z;
                }else{
                    y=z;
                }
            }while(z<4);
        }
        //產生新數字
        public void MakeNumber(){
            //System.out.print( "MakeNumber" );
            int d=0;
            do{
                d++;
                int y = (int) ( Math.random()*4);
                int z = (int) ( Math.random()*4 );
                if( b[y][z]==0 ) {
                    b[y][z]=(int)(Math.pow(2,level));
                    level=8;
                }else {
                    d--;
                }
            }while ( d < much );
            much=1; //(int)(Math.random()*5+1)(int)( Math.random()*2+1 );
        }
        //查看有沒有輸    上下左右移動後看是不是跟移動前相同且沒空格
        public boolean checklose(){
            //System.out.print( "checklose" );
            move("u");
            int n=0;
            checkzero();
            if( check() && checkzero()==0 ) n++;
            move("d");
            if( check() && checkzero()==0) n++;
            move("l" );
            if( check() && checkzero()==0) n++;
            move("r");
            if( check() && checkzero()==0) n++;
            if(n>=4) return true;
            else return false;
        }
        //備份陣列
        public void SaveOld(){
            for( int y=0 ; y <4 ; y++ ){
                for( int z=0 ; z <4 ; z++ ){
                    oldb[y][z]=b[y][z];
                }
            }
        }
        //查看備份前後是否相同
        public boolean check(){
            boolean same=true;
            for( int r=0 ; r <4 ; r++ ){
                for( int t=0 ; t <4 ; t++ ){
                    if( oldb[r][t]!=b[r][t] ) same=false;
                }
            }
            return same;
        }
        //還原備份
        public void reOld(){
            for( int y=0 ; y <4 ; y++ ){
                for( int z=0 ; z <4 ; z++ ){
                    b[y][z]=oldb[y][z];
                }
            }
        }
        //查看有幾個空格
        public int checkzero(){
            int zero=0;
            for( int y=0 ; y <4 ; y++ ) {
                for (int z = 0; z < 4; z++) {
                    if( b[y][z]==0 ) zero++;
                }
            }
            return zero;
        }
        //確認贏
        public boolean win() {
        	boolean win=false;
        	for(int i=0;i<4;i++) {
        		for(int j=0;j<4;j++) {
        			if(b[i][j]==2048) {
        				win=true;
        			}
        		}
        	}
        	return win;
        }
    }
    
    //計算遊戲時間
    public static class gametime extends Thread{
    	@Override
    	public void run() {
    		int s=0;
    		int m=0;
    		int h=0;
    		for(int i=0;i<Integer.MAX_VALUE;i++) {
    			s+=1;
    			if(s/60>=1) {
    				s=s%60;
    				m=s/60;
    			}
    			if(m/60>=1) {
    				m=m%60;
    				h=m/60;
    			}
    			try {	
    				Thread.sleep(500);
    			}catch(InterruptedException e) {
    				if(h>0) System.out.println("遊戲時間："+h+"時"+m+"分"+s+"秒");
    				else if(h==0&&m>0) System.out.println("遊戲時間："+m+"分"+s+"秒");
    				else if(h==0&&m==0&&s>0) System.out.println("遊戲時間："+s+"秒");
    				return;
    			}
    		}
    	}
    }
}

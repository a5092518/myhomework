import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Server {
	static ArrayList<String> name = new ArrayList<>();
	static ArrayList<Mythread> myth = new ArrayList<>();
	static ServerSocket server;
	static Socket socket;
	static DataInputStream instream;
	static DataOutputStream outstream;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			server=new ServerSocket(8000);
			System.out.println("*聊天室已開啟*");
			while(true) {
			socket=server.accept();
			
			instream = new DataInputStream(socket.getInputStream());
//			outstream = new DataOutputStream(socket.getOutputStream());
			Scanner sc = new Scanner(System.in);
			
				String user = instream.readUTF();
				System.out.println(user+" 加入聊天室。");
				name.add(user);
				
				Mythread mythread = new Mythread(user,socket);
				myth.add(mythread);
				mythread.start();
				for(int i=0;i<name.size();i++) {
					if(name.get(i).equals(user)) {
						myth.get(i).outstream.writeUTF("你加入了聊天室。");
						continue;
					}else {
						myth.get(i).outstream.writeUTF(user+"加入聊天室。");
					}
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static class Mythread extends Thread{
		String n;
		Socket socket;
		DataInputStream instream;
		DataOutputStream outstream;
		Mythread(String user, Socket s) throws IOException {
			// TODO Auto-generated constructor stub
			n=user;socket=s;
			outstream = new DataOutputStream(socket.getOutputStream());
			instream = new DataInputStream(socket.getInputStream());
		}
		public void run() {
			try {
				while(true) {
					String msg=instream.readUTF();		
					for(int i=0;i<name.size();i++) {
						if(name.get(i).equals(n)) {
							continue;
						}else {
							myth.get(i).outstream.writeUTF(n+msg);
						}	
					}
					if(msg.equals("已離開聊天室。")) {
						System.out.println(n+msg);
						break;
					}
				}
				//socket.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

}

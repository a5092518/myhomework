import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	static Socket socket;
	static DataInputStream instream;
	static DataOutputStream outstream;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			socket = new Socket("127.0.0.1",8000);
			instream = new DataInputStream(socket.getInputStream());
			outstream = new DataOutputStream(socket.getOutputStream());
			Scanner sc=new Scanner(System.in);
			th th=new th();
			System.out.println("請輸入暱稱：");
			String sendmsg=sc.next();
			outstream.writeUTF(sendmsg);
			th.start();
			while(true) {
				System.out.println("輸入訊息：");
				 sendmsg=sc.next();
				 
				 if(sendmsg.equals("exit")) {
					 sendmsg="已離開聊天室。";
					 outstream.writeUTF(sendmsg);
					 System.out.println("你已離開聊天室。");
					 break;
				 }else {
					 outstream.writeUTF(sendmsg);
				 }
			}
			instream.close();
			socket.close();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	static class th extends Thread{
		@Override
		public void run() {
			try {
				while(true) {
					String getmsg=instream.readUTF();
					System.out.println(getmsg);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

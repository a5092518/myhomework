package com.example.practice2_0730;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String [] name={"陳振東","馬麗菁","張朝旭","温敏淦","李志成"};
    int [] pic={R.drawable.teacher03,R.drawable.teacher04,R.drawable.teacher05,R.drawable.teacher06,R.drawable.teacher09};
    String [] cell={"037-381515","037-381510","037-381520","037-381514","037-381506"};
    String [] work={"教授","教授 兼 管理學院院長","副教授 兼 圖書館系統管理組長","副教授 兼 系主任","副教授 兼教學發展中心主任"};
    int [] num={0,0,0,0,0};
    ListView list1;
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list1=(ListView)findViewById(R.id.list1);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                num[i]++;
                Toast.makeText(MainActivity.this,name[i]+"老師，被點擊了"+num[i]+"次!",Toast.LENGTH_SHORT).show();
            }
        });
        BaseAdapter adpter=new BaseAdapter(){

            @Override
            public int getCount() {
                return name.length;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View layout=View.inflate(MainActivity.this,R.layout.layout1,null);
                ImageView img=(ImageView)layout.findViewById(R.id.img);
                TextView textView1=(TextView)layout.findViewById(R.id.tv1);
                TextView textView2=(TextView)layout.findViewById(R.id.tv2);
                TextView textView3=(TextView)layout.findViewById(R.id.tv3);
                img.setImageResource(pic[i]);
                textView1.setText(name[i]);
                textView2.setText(work[i]);
                textView3.setText(cell[i]);
                return layout;
            }
        };
        list1.setAdapter(adpter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        int id = item.getItemId();
        if (id == R.id.item1){
            Toast.makeText(MainActivity.this,"用來寄信的!",Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.item2){
            Toast.makeText(MainActivity.this,"用來打電話的!",Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.item3){
            Toast.makeText(MainActivity.this,"可以查到關於這個APP的資訊!",Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.item4){
            Toast.makeText(MainActivity.this,"可以設定這個APP的功能!",Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu1,menu);
        return true;
    }


}
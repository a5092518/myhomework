package com.example.practice0730;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView list1;
    String [] data={"BTS","GFRIEND","SEVENTEEN","IZ*ONE","BLACKPINK"};
    String [] data2={"珍珠奶茶","多多綠","紅茶拿鐵","冬瓜鮮奶"};
    ArrayAdapter arrd;

    AlertDialog.Builder B;
    AlertDialog D;
    Spinner spin1;
    TextView text1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.globalnetwork);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        list1=(ListView)findViewById(R.id.list1);
        arrd=new ArrayAdapter(this,android.R.layout.simple_list_item_1,data);
        list1.setAdapter(arrd);
        B=new AlertDialog.Builder(MainActivity.this);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(MainActivity.this,"點選"+data[i]+"位於第"+(i+1)+"列",Toast.LENGTH_SHORT).show();
                B.setTitle("提示");
                B.setMessage("點選的是："+data[i]);
                D=B.create();
                D.show();
            }
        });
        spin1=(Spinner)findViewById(R.id.spin1);
        ArrayAdapter<String>arrd2=new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,data2);
        spin1.setAdapter(arrd2);
        text1=findViewById(R.id.text1);
        spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                text1.setText("您點選的是："+data2[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        @Override
        public boolean onOptionsItemSelected(MenuItem item){
            switch (item.getItemId()) {
                case android.R.id.home: //對應到他的id 
                    finish();  //當點擊這個按鈕就讓他退出
                    return true;
            }
            int id = item.getItemId();
            if (id == R.id.id2) {
                Toast.makeText(MainActivity.this,"它是用來打電話的!",Toast.LENGTH_SHORT).show();
                return true;
            }
            if (id == R.id.id1) {
                Toast.makeText(MainActivity.this,"它可以用來設定!",Toast.LENGTH_SHORT).show();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu, menu);
            return true;
        }



}

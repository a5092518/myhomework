import java.util.Calendar;
import java.util.Scanner;


class myThread1 extends Thread{
	@Override
	public void run() {
		int s=0;
		int m=0;
		int h=0;
		for(int i=0;i<Integer.MAX_VALUE;i++) {
			s+=1;
			if(s/60>=1) {
				s=s%60;
				m=s/60;
			}
			if(m/60>=1) {
				m=m%60;
				h=m/60;
			}
			try {	
				Thread.sleep(500);
			}catch(InterruptedException e) {
				if(h>0) System.out.println("總時長花費："+h+"時"+m+"分"+s+"秒");
				else if(h==0&&m>0) System.out.println("總時長花費："+m+"分"+s+"秒");
				else if(h==0&&m==0&&s>0) System.out.println("總時長花費："+s+"秒");
				return;
			}
		}
	}
}

class myThread2 extends Thread{
	private int n=1;
	public void change(int a) {
		n=a;
	}
	@Override
	public void run() {
		
		for(int i=0;i<1000000;i++) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(System.currentTimeMillis());
			int hour= c.get(Calendar.HOUR_OF_DAY);
			int minute=c.get(Calendar.MINUTE);
			int second=c.get(Calendar.SECOND);
			System.out.println(hour+":"+minute+":"+second);
			try {	
				Thread.sleep(1000*n);
			}catch(InterruptedException e) {
				System.out.println("The end!");
				return;
			}
		}
		
	}
}

class myThread3 extends Thread{
	@Override
	public void run() {
		for(int i=0;i<600;i++) {
			main0722.Test.n+=1;
			System.out.println(this.getName()+":"+main0722.Test.n);
		}
	}
}

class myThread4 extends Thread{
	@Override
	public void run() {
		synchronized(main0722.count) {
			for(int i=0;i<600;i++) {
				main0722.count.n+=1;
				System.out.println(this.getName()+":"+main0722.count.n);
//				try {	
//					Thread.sleep(1000);
//				}catch(InterruptedException e) {
//					return;
//				}
			}
		}
	}
}

public class main0722 {
	public static class Test{
		static int n = 0;
	}
	public static class Count{
		int n;
	}
	static Thread thread3=new myThread3();
	static Thread thread4=new myThread3();
	static Thread thread=new myThread1();
	static Thread thread2=new myThread2();
	
	static Thread thread5=new myThread4();
	static Thread thread6=new myThread4();
	static Count count = new Count();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		//第一題
		int ans=(int)(Math.random()*100+1);
		int min=1,max=100;
		System.out.println(ans);
		thread.start();
		System.out.println("請猜一個數字：");
		int userAns=sc.nextInt();
		if(userAns>0) {
			while(userAns!=ans) {
				if(userAns>ans) {
					max=userAns-1;
					if(max==ans&&min==ans) {
						System.out.println("因為只剩一個數字，所以你輸了喔!答案是："+ans);
						break;
					}
					System.out.println("太大了!");
					System.out.println(min+"~"+max+"請重新猜一次：");
					userAns=sc.nextInt();
				}else if(userAns<ans) {
					min=userAns+1;
					if(min==ans&&max==ans) {
						System.out.println("因為只剩一個數字，所以你輸了喔!答案是："+ans);
						break;
					}
					System.out.println("太小了!");
					System.out.println(min+"~"+max+"請重新猜一次：");
					userAns=sc.nextInt();
				}
			}
			if(userAns==ans) {
				thread.interrupt();
				System.out.println("恭喜你答對了!");
				
				
				
			}
		}else {
			thread.interrupt();
			System.out.println("您輸入的數字需大於0!");
			
		}
		//第二題
//		thread2.start();
//		String n=" ";
//		int N=0;
//		do {
//			System.out.println("請輸入數字：");
//			n=sc.next();
//			if(n.equals("stop")) {
//				thread2.interrupt();
//				break;
//			}else N=Integer.parseInt(n);
//			((myThread2) thread2).change(N);
//		}while(!n.equals("stop"));
		//第三題-1
		thread3.start();
		thread4.start();
		//第三題-2
//		thread5.start();
//		thread6.start();
	}

}
